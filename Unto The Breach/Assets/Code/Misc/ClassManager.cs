﻿using UnityEngine;
using System.Collections.Generic;

namespace UB
{
    public class ClassManager : MonoBehaviour
    {
        public static ClassManager Instance { get; private set; }

        public PlayerClass[] availableClasses;

        private void Awake()
        {
            Instance = this;
        }

        [System.Serializable]
        public struct PlayerClass
        {
            public string name;
            public List<vp_UnitBankType> primaryWeapons;
            public List<vp_UnitBankType> secondaryWeapons;
        }
    }
}