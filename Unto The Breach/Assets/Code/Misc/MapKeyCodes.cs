﻿using System.Collections.Generic;

public static class MapKeyCodes
{
    private static Dictionary<int, string> mapKeyCodes;

    public static void Setup()
    {
        mapKeyCodes = new Dictionary<int, string>();
        mapKeyCodes.Add(2, "Night Raid");
    }

    public static string GetValue(int key)
    {
        return mapKeyCodes[key];
    }
}