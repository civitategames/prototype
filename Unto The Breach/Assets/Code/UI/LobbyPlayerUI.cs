﻿using UnityEngine;
using UnityEngine.UI;

namespace UB.UI
{
    public class LobbyPlayerUI : MonoBehaviour
    {
        [SerializeField]
        private Text nameText;

        [SerializeField]
        private Image readyImage;

        private PhotonPlayer refPlayer;

        private void Awake()
        {
            readyImage.gameObject.SetActive(false);
        }

        public void Setup(PhotonPlayer player)
        {
            if(player == null)
            {
                nameText.text = "<EMPTY>";
                readyImage.gameObject.SetActive(false);
                refPlayer = null;
                return;
            }

            refPlayer = player;
            nameText.text = player.NickName;
        }

        public void UpdateReadyMark()
        {
            if(refPlayer == null)
            {
                readyImage.gameObject.SetActive(false);
            }
            else
            {
                bool ready = false;
                bool.TryParse(refPlayer.CustomProperties["ready"].ToString(), out ready);
                readyImage.gameObject.SetActive(ready);
            }
        }
    }
}