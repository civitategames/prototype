﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace UB.UI
{
    public class ServerUI : Photon.MonoBehaviour
    {
        [SerializeField]
        private InputField roomNameInput;

        [SerializeField]
        private Slider playerCountSlider;

        [SerializeField]
        private Button createRoomButton;

        [SerializeField]
        private Text statusText;

        [SerializeField]
        private Text maxPlayerCountText;

        private ServerItemUI[] itemUIs;

        private void Awake()
        {
            itemUIs = GetComponentsInChildren<ServerItemUI>();
        }

        private void Start()
        {
            playerCountSlider.value = 8;
            statusText.text = "Connecting...";
            Connect();
            createRoomButton.onClick.AddListener(CreateRoom);
            playerCountSlider.onValueChanged.AddListener(UpdatePlayerCountText);
            SetupCustomPlayerProperties();
        }

        private void UpdatePlayerCountText(float value)
        {
            maxPlayerCountText.text = "Max Players: " + value.ToString("F0");
        }

        private void UpdateUI()
        {
            for(int i = 0; i < itemUIs.Length; i++)
            {
                itemUIs[i].Hide();
            }

            RoomInfo[] rooms = PhotonNetwork.GetRoomList();

            createRoomButton.interactable = rooms.Length < 6 && PhotonNetwork.connected; // we can only allow up to 6 rooms currently
            UpdatePlayerCountText(playerCountSlider.value);

            for (int i = 0; i < rooms.Length; i++)
            {
                if(i < itemUIs.Length)
                {
                    itemUIs[i].Setup(rooms[i]);
                }
            }
        }

        private void Connect()
        {
            PhotonNetwork.ConnectUsingSettings(vp_Gameplay.Version);
            UpdateUI();
        }

        private void CreateRoom()
        {
            if(roomNameInput.text != "")
            {
                // double check to make sure room doesn't already exist
                RoomInfo[] rooms = PhotonNetwork.GetRoomList();

                foreach(RoomInfo room in rooms)
                {
                    if(room.Name == roomNameInput.text)
                    {
                        statusText.text = "Failed to Create Room (Room Name Taken)";
                        return;
                    }
                }

                RoomOptions options = new RoomOptions
                {
                    MaxPlayers = (byte)playerCountSlider.value,
                    CustomRoomPropertiesForLobby = new string[] { "m" },
                    CustomRoomProperties = new Hashtable()
                };

                options.CustomRoomProperties.Add("m", "2");
                DisableAll();
                statusText.text = "Creating Room...";

                if (!PhotonNetwork.CreateRoom(roomNameInput.text, options, TypedLobby.Default))
                {
                    statusText.text = "Unable to Create Room!";
                }
            }
        }

        /// <summary>
        /// Initialize the player default properties, needs to be setup before entering aroom
        /// </summary>
        private void SetupCustomPlayerProperties()
        {
            // add properties if they dont exist
            if (PhotonNetwork.player.CustomProperties == null)
            {
                PhotonNetwork.player.CustomProperties = new Hashtable();
            }

            Hashtable properties = PhotonNetwork.player.CustomProperties;

            // TODO: Add any custom properties here
            // set team property
            if(properties.ContainsKey("team"))
            {
                properties["team"] = 0; // no team
            }
            else
            {
                properties.Add("team", 0);
            }

            // set ready property
            if(properties.ContainsKey("ready"))
            {
                properties["ready"] = "false"; // not ready by default
            }
            else
            {
                properties.Add("ready", "false");
            }

            // set primary weapon
            if(!properties.ContainsKey("primary"))
            {
                properties.Add("primary", "");
            }

            Debug.Log(PhotonNetwork.player.CustomProperties);
            PhotonNetwork.player.SetCustomProperties(properties, null, true);
        }

        public void DisableAll()
        {
            createRoomButton.interactable = false;
            playerCountSlider.interactable = false;

            for (int i = 0; i < itemUIs.Length; i++)
            {
                itemUIs[i].Disable();
            }
        }

        public virtual void OnConnectedToPhoton()
        {
            statusText.text = "Connected To Server";
            UpdateUI();
        }

        public virtual void OnReceivedRoomListUpdate()
        {
            UpdateUI();
        }

        public virtual void OnCreatedRoom()
        {
            int mapKey = int.Parse(PhotonNetwork.room.CustomProperties["m"].ToString());
            SceneManager.LoadScene(mapKey);
        }
    }
}