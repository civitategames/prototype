﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace UB.UI
{
    public class ServerItemUI : Photon.MonoBehaviour
    {
        [SerializeField]
        private Text roomName;

        [SerializeField]
        private Text mapName;

        [SerializeField]
        private Text playerCount;

        [SerializeField]
        private Button connectButton;

        private ServerUI uiParent;
        private string refRoomName;
        private int refMapKey;

        private void Awake()
        {
            uiParent = GetComponentInParent<ServerUI>();
        }

        private void Start()
        {
            connectButton.onClick.AddListener(() => 
            {
                uiParent.DisableAll();
                PhotonNetwork.JoinRoom(refRoomName);
            });
        }

        public void Setup(RoomInfo room)
        {
            refRoomName = room.Name;
            refMapKey = int.Parse(room.CustomProperties["m"].ToString());
            roomName.text = room.Name;
            mapName.text = MapKeyCodes.GetValue(refMapKey);
            playerCount.text = room.PlayerCount.ToString() + "/" + room.MaxPlayers.ToString();
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Disable()
        {
            connectButton.interactable = false;
        }

        public void Enable()
        {
            connectButton.interactable = true;
        }

        private void OnJoinedRoom()
        {
            SceneManager.LoadScene(refMapKey);
        }
    }
}