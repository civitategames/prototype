﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UB.Networking;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace UB.UI
{
    public class LobbyUI : Photon.MonoBehaviour
    {
        public static LobbyUI Instance { get; private set; }

        [SerializeField]
        private Button readyButton;

        [SerializeField]
        private Button joinTeam1Button;

        [SerializeField]
        private Button joinTeam2Button;

        [SerializeField]
        private Button nextPrimaryWeaponButton;

        [SerializeField]
        private Button previousPrimaryWeaponButton;

        [SerializeField]
        private Button nextSecondaryWeaponButton;

        [SerializeField]
        private Button previousSecondaryWeaponButton;

        [SerializeField]
        private Button nextClassButton;

        [SerializeField]
        private Button previousClassButton;

        [SerializeField]
        private RawImage primaryWeaponImage;

        [SerializeField]
        private RawImage secondaryWeaponImage;

        [SerializeField]
        private Text className;

        [SerializeField]
        private Text levelName;

        [SerializeField]
        private Text primaryWeaponText;

        [SerializeField]
        private Text secondaryWeaponText;

        [SerializeField]
        private LobbyPlayerUI[] playerUIs;

        [SerializeField]
        private LobbyPlayerUI[] team1UIs;

        [SerializeField]
        private LobbyPlayerUI[] team2UIs;

        // private variables
        private int classChoice = 0;
        private int primaryWeaponChoice = 0;
        private int secondaryWeaponChoice = 0;
        private int teamChoice = 0; // local variable only
        private bool isReady = false;
        
        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            levelName.text = SceneManager.GetActiveScene().name;
            UpdateUI();
            UpdateWeaponChoice();
            readyButton.onClick.AddListener(Ready);

            joinTeam1Button.onClick.AddListener(() => 
            {
                JoinTeam(1);
            });

            joinTeam2Button.onClick.AddListener(() =>
            {
                JoinTeam(2);
            });

            nextPrimaryWeaponButton.onClick.AddListener(() =>
            {
                NextPrimaryWeapon(1);
            });

            previousPrimaryWeaponButton.onClick.AddListener(() =>
            {
                NextPrimaryWeapon(-1);
            });

            nextSecondaryWeaponButton.onClick.AddListener(() => 
            {
                NextSecondaryWeapon(1);
            });

            previousSecondaryWeaponButton.onClick.AddListener(() =>
            {
                NextSecondaryWeapon(-1);
            });

            nextClassButton.onClick.AddListener(() =>
            {
                NextClass(1);
            });

            previousClassButton.onClick.AddListener(() =>
            {
                NextClass(-1);
            });
        }

        private void Ready()
        {
            // set player properties to ready
            isReady = !isReady;
            Hashtable table = PhotonNetwork.player.CustomProperties;
            table["ready"] = isReady.ToString();
            table["primary"] = PlayerStats.primaryWeapon;
            PhotonNetwork.player.SetCustomProperties(table, null, true);
            Debug.Log(PhotonNetwork.player.CustomProperties);
            photonView.RPC("PlayerReady", PhotonTargets.AllBuffered, PhotonNetwork.player, isReady);
        }

        private void JoinTeam(int team)
        {
            int oldTeam = int.Parse(PhotonNetwork.player.CustomProperties["team"].ToString());
            LobbyTeamManager.Instance.LeaveTeam(PhotonNetwork.player, oldTeam);
            LobbyTeamManager.Instance.JoinTeam(PhotonNetwork.player, team);
            teamChoice = team;
            UpdateWeaponChoice();
        }

        public void UpdateUI()
        {
            //tracks position of team members
            int team0Index = 0; // no team
            int team1Index = 0; // team 1
            int team2Index = 0; // team 2

            // setup no team player UIs
            foreach(PhotonPlayer player in PhotonNetwork.playerList)
            {
                int teamNumber = int.Parse(player.CustomProperties["team"].ToString());

                if (teamNumber == 0)
                {
                    playerUIs[team0Index].Setup(player);
                    team0Index++;
                }
                else if(teamNumber == 1)
                {
                    team1UIs[team1Index].Setup(player);
                    team1Index++;
                }
                else if(teamNumber == 2)
                {
                    team2UIs[team2Index].Setup(player);
                    team2Index++;
                }
            }

            // update player UI objects for non-teamed players
            for(int i = 0; i < playerUIs.Length; i++)
            {
                playerUIs[i].UpdateReadyMark();

                if(i >= PhotonNetwork.room.MaxPlayers)
                {
                    playerUIs[i].gameObject.SetActive(false);
                    continue;
                }
                else if(i >= team0Index)
                {
                    playerUIs[i].Setup(null);
                    continue;
                }
            }

            // update team 1 uis
            for(int i = 0; i < team1UIs.Length; i++)
            {
                team1UIs[i].UpdateReadyMark();

                if (i >= PhotonNetwork.room.MaxPlayers)
                {
                    team1UIs[i].gameObject.SetActive(false);
                    continue;
                }
                else if (i >= team1Index)
                {
                    team1UIs[i].Setup(null);
                    continue;
                }
            }

            // update team 2 uis
            for (int i = 0; i < team2UIs.Length; i++)
            {
                team2UIs[i].UpdateReadyMark();

                if (i >= PhotonNetwork.room.MaxPlayers)
                {
                    team2UIs[i].gameObject.SetActive(false);
                    continue;
                }
                else if (i >= team2Index)
                {
                    team2UIs[i].Setup(null);
                    continue;
                }
            }

            // disable ready button unless player is on a team
            readyButton.interactable = int.Parse(PhotonNetwork.player.CustomProperties["team"].ToString()) != 0;
        }

        private void UpdateWeaponChoice()
        {
            if(teamChoice == 0)
            {
                primaryWeaponImage.gameObject.SetActive(false);
                primaryWeaponText.text = "";
                secondaryWeaponImage.gameObject.SetActive(false);
                secondaryWeaponText.text = "";
            }
            else
            {
                // setup primary weapon slot
                primaryWeaponImage.gameObject.SetActive(true);
                primaryWeaponImage.texture = ClassManager.Instance.availableClasses[classChoice].primaryWeapons[primaryWeaponChoice].Icon;
                primaryWeaponText.text = ClassManager.Instance.availableClasses[classChoice].primaryWeapons[primaryWeaponChoice].DisplayName;

                // setup secondary weapon slot
                secondaryWeaponImage.gameObject.SetActive(true);
                secondaryWeaponImage.texture = ClassManager.Instance.availableClasses[classChoice].secondaryWeapons[secondaryWeaponChoice].Icon;
                secondaryWeaponText.text = ClassManager.Instance.availableClasses[classChoice].secondaryWeapons[secondaryWeaponChoice].DisplayName;

                // setup class slot
                className.text = ClassManager.Instance.availableClasses[classChoice].name;

                PlayerStats.primaryWeapon = ClassManager.Instance.availableClasses[classChoice].primaryWeapons[primaryWeaponChoice].name;
                PlayerStats.secondaryWeapon = ClassManager.Instance.availableClasses[classChoice].secondaryWeapons[secondaryWeaponChoice].name;
                PlayerStats.className = ClassManager.Instance.availableClasses[classChoice].name;
            }
        }

        private void NextPrimaryWeapon(int direction)
        {
            primaryWeaponChoice += direction;

            if (primaryWeaponChoice < 0)
            {
                primaryWeaponChoice = ClassManager.Instance.availableClasses[classChoice].primaryWeapons.Count - 1;
            }
            else if (primaryWeaponChoice == ClassManager.Instance.availableClasses[classChoice].primaryWeapons.Count)
            {
                primaryWeaponChoice = 0;
            }

            UpdateWeaponChoice();
        }

        private void NextSecondaryWeapon(int direction)
        {
            secondaryWeaponChoice += direction;

            if (secondaryWeaponChoice < 0)
            {
                secondaryWeaponChoice = ClassManager.Instance.availableClasses[classChoice].secondaryWeapons.Count - 1;
            }
            else if (secondaryWeaponChoice == ClassManager.Instance.availableClasses[classChoice].secondaryWeapons.Count)
            {
                secondaryWeaponChoice = 0;
            }

            UpdateWeaponChoice();
        }

        private void NextClass(int direction)
        {
            classChoice += direction;

            if (classChoice < 0)
            {
                classChoice = ClassManager.Instance.availableClasses.Length - 1;
            }
            else if (classChoice == ClassManager.Instance.availableClasses.Length)
            {
                classChoice = 0;
            }

            UpdateWeaponChoice();
        }

        private void OnPhotonPlayerDisconnected(PhotonPlayer player)
        {
            UpdateUI();
        }

        private void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            UpdateUI();
        }
        
        [PunRPC]
        private void PlayerReady(PhotonPlayer player, bool ready)
        {
            UpdateUI();

            // send ready status to master client
            photonView.RPC("UpdatePlayerReady", PhotonTargets.MasterClient);
        }

        /// <summary>
        /// When players ready up they send their ready status to the master client
        /// When every player is ready the master client moves everyone to the "Game"
        /// </summary>
        /// <param name="id"></param>
        /// <param name="enabled"></param>
        [PunRPC]
        private void UpdatePlayerReady()
        {
            if(vp_Gameplay.IsMaster)
            {
                int readyCount = 0;

                foreach (PhotonPlayer player in PhotonNetwork.playerList)
                {
                    bool ready = false;
                    bool.TryParse(player.CustomProperties["ready"].ToString(), out ready);

                    if (ready)
                    {
                        readyCount++;
                    }
                }

                if (readyCount == PhotonNetwork.playerList.Length && PhotonNetwork.playerList.Length >= 2)
                {
                    // TODO: Add a property to the room to let joining clients know the game is in progress
                    photonView.RPC("HideLobbyUI", PhotonTargets.All);
                }
            }
        }

        [PunRPC]
        private void HideLobbyUI()
        {
            gameObject.SetActive(false);
            FindObjectOfType<vp_MPMaster>().photonView.RPC("RequestInitialSpawnInfo", PhotonTargets.MasterClient, PhotonNetwork.player, 0, name);
        }
    }
}