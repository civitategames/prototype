﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace UB.UI
{
    public class MainMenuUI : Photon.MonoBehaviour
    {
        public static int disconnectFlag = 0; // track the flag for if the player was disconnected from server

        [SerializeField]
        private InputField usernameInput;

        [SerializeField]
        private Button loginButton;

        [SerializeField]
        private Button settingsButton;

        [SerializeField]
        private Button creditsButton;

        [SerializeField]
        private Button closeWindowButton;

        [SerializeField]
        private Button quitButton;

        [SerializeField]
        private GameObject disconnectedWindow;

        private void Start()
        {
            MapKeyCodes.Setup();
            loginButton.onClick.AddListener(JoinLobby);
            quitButton.onClick.AddListener(Application.Quit);

            closeWindowButton.onClick.AddListener(() => 
            {
                disconnectedWindow.SetActive(false);
            });

            if(disconnectFlag == 1)
            {
                disconnectedWindow.SetActive(true);
            }
        }

        private void JoinLobby()
        {
            if(usernameInput.text != "")
            {
                vp_Gameplay.PlayerName = usernameInput.text;
                SceneManager.LoadScene("02_SERVER");
            }
        }

        protected virtual void OnEnable()
        {
            vp_GlobalEvent.Register("DisableMultiplayerGUI", delegate () { vp_Utility.Activate(gameObject, false); });  // called from 'vp_MPSinglePlayerTest'
            vp_GlobalEvent.Register("Disconnected", Reset);         // called from 'vp_MPConnection' when player disconnects
        }

        protected virtual void OnDisable()
        {
            vp_GlobalEvent.Unregister("DisableMultiplayerGUI", delegate () { vp_Utility.Activate(gameObject, false); });
            vp_GlobalEvent.Unregister("Disconnected", Reset);
        }

        public void Reset()
        {
            vp_MPConnection.StayConnected = false;
            GUI.FocusControl("1");
            vp_Utility.LockCursor = false;
        }
    }
}