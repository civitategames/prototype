﻿using UnityEngine;

namespace UB.Items.Weapons
{
    public class FPWeaponFix : MonoBehaviour
    {
        public Vector3 refPosition;
        public Vector3 refRotation;

        public void Fix()
        {
            transform.localPosition = refPosition;
            transform.localEulerAngles = refRotation;
        }
    }
}