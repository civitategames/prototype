﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace UB.Networking
{
    public class NetworkStatus : Photon.MonoBehaviour
    {
        public static NetworkStatus Instance { get; private set; }

        private void Awake()
        {
            // if the user returns to the main menu, make sure to delete the copy that already exists there
            if(Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            vp_Gameplay.IsMultiplayer = true;
            DontDestroyOnLoad(gameObject);
        }

        private void OnJoinedLobby()
        {
            PhotonNetwork.player.NickName = vp_Gameplay.PlayerName;
        }

        private void OnJoinedRoom()
        {
            vp_Gameplay.IsMaster = PhotonNetwork.isMasterClient;
        }

        private void OnPhotonPlayerDisconnected(PhotonPlayer player)
        {
            vp_Gameplay.IsMaster = PhotonNetwork.isMasterClient;
        }

        private void OnDisconnectedFromPhoton()
        {
            UB.UI.MainMenuUI.disconnectFlag = 1;
            SceneManager.LoadScene("01_MAIN_MENU");
        }
    }
}