﻿using UnityEngine;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UB.UI;

namespace UB.Networking
{
    public class LobbyTeamManager : Photon.MonoBehaviour
    {
        public static LobbyTeamManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        public void JoinTeam(PhotonPlayer player, int team)
        {
            Hashtable table = player.CustomProperties;
            table["team"] = team;
            player.SetCustomProperties(table, null, true);
            photonView.RPC("PlayerJoinedTeam", PhotonTargets.All);
        }

        public void LeaveTeam(PhotonPlayer player, int team)
        {
            Hashtable table = player.CustomProperties;
            table["team"] = -1;
            player.SetCustomProperties(table, null, true);
            photonView.RPC("PlayerLeftTeam", PhotonTargets.All);
        }

        [PunRPC]
        private void PlayerJoinedTeam()
        {
            LobbyUI.Instance.UpdateUI();
        }

        [PunRPC]
        private void PlayerLeftTeam()
        {
            LobbyUI.Instance.UpdateUI();
        }

        private void OnPhotonPlayerDisconnected(PhotonPlayer player)
        {
            int oldTeam = int.Parse(player.CustomProperties["team"].ToString());
            LeaveTeam(player, oldTeam);
        }

        private void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            JoinTeam(newPlayer, 0);   
        }
    }
}